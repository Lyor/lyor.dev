import { useEffect, useState } from 'react'
import { getProjects } from '../service'
import { Project } from '../service/types'

export const useGetProjects = () => {
  const [projects, setProjects] = useState<Project[] | []>([])

  useEffect(() => {
    // getProjectList
    setProjects(getProjects())
  }, [])

  return {
    projects,
  }
}
