
export interface Project  {
    id: number;
    githubLink: string;
    name: string;
    image?: string
}

export interface Projects {
    projects: Project[]
}