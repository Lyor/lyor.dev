import styles from './index.module.scss'
import { Title } from '../../../ui/navigation/title'
import { useGetProjects } from '../hooks'
import { ProjectList } from '../components/projectList'

export const ProjectsPage = () => {
  const { projects } = useGetProjects()
  return (
    <div className={styles.projectWrapper}>
      <Title title='Description des réalisations' />
      <ProjectList data={projects} />
    </div>
  )
}
