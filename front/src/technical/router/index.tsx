import { Routes, Route } from 'react-router-dom'
import { Landing } from '../../business/Landing'
import { ProjectsPage } from '../../business/projects/page'

export const Router = () => {
  return (
    <div>
      <Routes>
        <Route path='/' element={<Landing />} />
        <Route path='/projects' element={<ProjectsPage />} />
      </Routes>
    </div>
  )
}
