import React from 'react'

import './App.scss'
import { Router } from './technical/router'
import { Navigation } from './ui/navigation'

function App() {
  return (
    <div className='App'>
      <Navigation />
      <Router />
    </div>
  )
}

export default App
