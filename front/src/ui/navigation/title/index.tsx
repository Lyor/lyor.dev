import styles from './index.module.scss'
interface TitleProps {
  title: string
}

export const Title = ({ title }: TitleProps) => {
  return (
    <div>
      <h2 className={styles.title}>{title}</h2>
    </div>
  )
}
