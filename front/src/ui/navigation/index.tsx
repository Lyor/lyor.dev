import { Link } from 'react-router-dom'
import styles from './index.module.scss'

export const Navigation = () => {
  return (
    <div className={styles.navigation}>
      <Link to={'/projects'}>My Projects</Link>
      <Link to={'/learning'}>Learning</Link>
      <Link to={'/resume'}>My Resume</Link>
    </div>
  )
}
